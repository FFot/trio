#---------Create and Provision Vault Instance--------
#
# # Create Vault Instance
# resource "aws_instance" "vault" {
#   instance_type = var.instance_type_a
#   ami           = var.ami
#   # Provision Jenkins instance with ansible playbook
#   user_data = file("userdata.sh")
#
#   tags = {
#     Name = "Jenkins"
#   }
#
#   key_name               = var.key
#   vpc_security_group_ids = ["${aws_security_group.jenkins_sg.id}"]
#   iam_instance_profile   = aws_iam_instance_profile.jenkins_access_profile.id
#   subnet_id              = aws_subnet.public_subnet_m.id
#
# }
