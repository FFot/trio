#------provider-------
variable "aws_region" {
  type = string
}
variable "aws_profile" {
  type = string
}

#--------VPC--------
data "aws_availability_zones" "available" {}
variable "vpc_cidr" {}
variable "cidrs" {
  type = map
}
variable "cidr_blocks" {}

variable "zone_id" {}

#--------ELB-----------
variable "elb_healthy_threshold" {}
variable "elb_unhealthy_threshold" {}
variable "elb_timeout" {}
variable "elb_interval" {}

#--------DNS-------------
variable "domain_name" {
  type = string
}

#------Instance---------
variable "instance_type_j" {
  type = string
}
variable "instance_type_a" {
  type = string
}
variable "key" {
  type = string
}
variable "ami" {
  type = string
}
variable "ami_bastion" {
  type = string
}

#--------Tags-------------
variable "tags_bastion" {
  default = {
    Name        = "Trio Bastion"
    Project     = "TrioAssessment3"
    description = "The Tags to be used by any item related to the Bastion host"
  }
}

variable "tags_key" {
  type = map(string)
  default = {
    Name        = "trioKey"
    Project     = "Assessment3"
    Description = "Key used by TrioTeam"
  }
}

#-------S3 Bucket--------
variable "s3_bucket" {
  type = string
}

#---------SSH Key-----------

# Variables used to generate SSH key
variable "ssh_public_key_path" {
  type        = string
  default     = "triosec"
  description = "Path to SSH public key directory"
}

variable "PATH_TO_PUBLIC_KEY" {
  type    = string
  default = "triosec/trioKey.pub"
}

variable "PATH_TO_PRIVATE_KEY" {
  type    = string
  default = "triosec/superKey.pem"
}

variable "generate_ssh_key" {
  type = bool
}

variable "ssh_key_algorithm" {
  type        = string
  default     = "RSA"
  description = "SSH key algorithm"
}

# Define file extensions for storing key details
variable "private_key_extension" {
  type        = string
  default     = ".pem"
  description = "Private key extension"
}

variable "public_key_extension" {
  type        = string
  default     = ".pub"
  description = "Public key extension"
}
