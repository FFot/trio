
###----- This script provisions a bastion vm to have SSH access to the DB server on the private subnet -----###

###--- Create Bastions for Each Region ---###
resource "aws_instance" "bastion" {
  ami                    = var.ami_bastion
  instance_type          = var.instance_type_a
  key_name               = var.key
  subnet_id              = aws_subnet.public_subnet_m.id
  iam_instance_profile   = aws_iam_instance_profile.jenkins_access_profile.id
  vpc_security_group_ids = ["${aws_security_group.bastion_sg.id}"]
  user_data              = <<-EOF
                             #!/bin/bash
                             yum install -y mysql56-server
                             yum install -y mysql
                           EOF
  tags                   = var.tags_bastion
}

output "bastion_public_dns" {
  value = aws_instance.bastion.public_dns
}


resource "aws_eip" "bastion_eip" {
  vpc = true
}

# Associate EIP to Bastion instance
resource "aws_eip_association" "bastion_eip_assoc" {
  depends_on    = [aws_internet_gateway.trio_internet_gateway]
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.bastion_eip.id
}
