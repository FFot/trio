#-------------Create VPC----------------

resource "aws_vpc" "trio_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  enable_classiclink   = false

  tags = {
    Name = "TrioVPC"
  }
}

#-----------Create VPC Subnets----------

#Public Management Subnet

resource "aws_subnet" "public_subnet_m" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["publicM"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "PublicSubnetM"
  }
}

#-------Create Internet gateway---------

resource "aws_internet_gateway" "trio_internet_gateway" {
  vpc_id = aws_vpc.trio_vpc.id

  tags = {
    Name = "TrioInternetGateway"
  }
}

#-------Create Route tables----------

#Public Route Table

resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.trio_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.trio_internet_gateway.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

#------------Create Subnet Associations---------------

# Management subnet association with Public Route Table
resource "aws_route_table_association" "public_assoc_m" {
  subnet_id      = aws_subnet.public_subnet_m.id
  route_table_id = aws_route_table.public_rt.id
}
