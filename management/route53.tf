
#-----------Route53-------------

# Jenkins A Record Set
resource "aws_route53_record" "jenkins" {
  depends_on = [aws_eip_association.jenkins_eip_assoc]
  zone_id    = var.zone_id
  name       = "www.jenkins.${var.domain_name}.co.uk"
  type       = "A"
  ttl        = "20"
  records    = [aws_eip.jenkins_eip.public_ip]
}

# Bastion A Record Set
resource "aws_route53_record" "bastion" {
  depends_on = [aws_eip_association.bastion_eip_assoc]
  zone_id    = var.zone_id
  name       = "bastion.${var.domain_name}.co.uk"
  type       = "A"
  ttl        = "20"
  records    = [aws_eip.bastion_eip.public_ip]
}
