# Add state to bucket
terraform {
  backend "s3" {
    bucket = "trio-s3-bucket"
    key    = "management/terraform.tfstate"
    region = "eu-west-2"
  }
}
