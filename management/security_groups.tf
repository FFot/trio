#-----------Create Security Groups---------

#Jenkins Security group

resource "aws_security_group" "jenkins_sg" {
  vpc_id      = aws_vpc.trio_vpc.id
  name        = "JenkinsSecurityGroup"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.cidr_blocks
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = var.cidr_blocks
  }
  tags = {
    Name = "JenkinsSecurityGroup"
  }
}

#Bastion Security group

resource "aws_security_group" "bastion_sg" {
  vpc_id      = aws_vpc.trio_vpc.id
  name        = "BastionSecurityGroup"
  description = "security group that allows ssh and all egress traffic"
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.cidr_blocks
  }

  tags = {
    Name = "BastionSecurityGroup"
  }
}
