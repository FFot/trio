#------------IAM ROLES----------------

#Create Jenkins access role profile

resource "aws_iam_instance_profile" "jenkins_access_profile" {
  name = "TrioJenkinsAccessProfile"
  role = aws_iam_role.jenkins_access_role.name
}

#Create Jenkins role policy

resource "aws_iam_role_policy" "jenkins_access_policy" {
  name = "TrioJenkinsAccessPolicy"
  role = aws_iam_role.jenkins_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Effect": "Allow",
    "Action":[
        "s3:*",
        "ec2:*",
        "iam:*",
        "rds:*",
        "elasticloadbalancing:*",
        "autoscaling:*"
            ],
    "Resource": "*"
        }
    ]
}
EOF
}

#Create Jenkins access role

resource "aws_iam_role" "jenkins_access_role" {
  name = "TrioJenkinsAccessRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
  },
      "Effect": "Allow",
      "Sid": ""
      }
    ]
}
EOF
}


#Create instance S3 access role profile

resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "TrioS3AccessProfile"
  role = aws_iam_role.s3_access_role.name
}

#Create instance role policy

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "TrioS3AccessPolicy"
  role = aws_iam_role.s3_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action":[
          "s3:*",
          "ec2:*"
          ],
      "Resource": "*"
    }
  ]
}
EOF
}

#Create instance S3 access role attached to instance

resource "aws_iam_role" "s3_access_role" {
  name = "TrioS3AccessRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
  },
      "Effect": "Allow",
      "Sid": ""
      }
    ]
}
EOF
}
