# **README** #

### **TRIO Team** ###
TRIO Team Assessment 3: Use Ansible and Terraform in order to create a pipeline for the client to be able to perform continuous delivery of their application (Petclinic) using cloud technology.


### **Team members:** ###
  * Cherry
  * Fani
  * Silviana

### **Description of the repository** ###
  - README.md ~> documentation on how to use the system, giving brief info about the project
  - .gitignore ~> files/folders that should not be and aren't included on the git repository are stated here
  - management ~> terraform files with scripts that create the Management Infrastructure including the VPC, management subnet, IAM roles, Bastion Host Jenkins security groups and SSH KEY as well as Jenkins.
  - git-jenkins ~> an ansible-playbook with various scripts that ensure the provisioning of Jenkins Server; the environments that can be used are local and dev; roles that can be used: ansible-role-java, ansible-role-jenkins, destroy config Jenkins, ec2, jenkins-apps, jenkins-jobs; the ec2 will not be used in our system described below as we use the ansible code only for provisioning and terraform for creating the Jenkins instance; the jenkins-jobs role can be used in order to update it with the xml files of the jobs created
  - jenkinsjobs ~> directory which includes scripts that creates the apps Infrastructure, runs the resillient database (from scratch or from a snapshot) and the ELB
  - petclinicrole ~> an ansible-playbook that creates the Jenkins Job for running PetClinic; the playbook creates a temporary ec2 instance (launch_ec2i role), provision it and test it in order to run PetClinic (provision_petclinic role), create the AMI role of the instance if PetClinic webserver works and terminates the instance storing the AMI ; still at a draft (developing) stage
  - Vault ~> ansible-playbook that enables password rotation that increases security of the database and/or jenkins; still at a draft (developing) stage
  - Note: dev branch includes system parts that are still under development while the other branches (infrastructure, jenkins, database, petclinic and vault) were used to develop various parts of the system

### **Documentation** ###
  1. Git clone the repository in the terminal:
      `git clone git@bitbucket.org:FFot/trio.git `
  2. Pre-step: the code for generating SSH Key only allows you to create a new key (if key pair already exists the code returns error). If you want to create the Management Infrastructure and the SSH key together - go to Amazon AWS and delete the Key Pair named 'trioKey'. Otherwise, if you want to use the already created key do the following command:
       `mv tri/management/sshkey.tf trio`.
    You must have an AWS credentials profile.
    This code requires an S3 bucket to be built manually, and the variable s3_bucket should be edited so that it matches the name of your s3 bucket. The S3 bucket must allow public read and write access and you must export your profile manually using:
     `export AWS_PROFILE=PROFILENAME`
     You must also have a hosted zone configured manually and this zone must also be changed in the variable called domain_name
  3. Create the Management Infrastructure, Configure and Launch the Jenkins Server and the Bastion Host:  
      `cd trio/management`
      `terraform init`
      `export AWS_PROFILE=(YOURAWSPROFILE)`
      `terraform apply -auto-approve`
  4. Create apps' Infrastructure and run the applications through Jenkins Jobs (PetClinic, Mariadb Database):
      Log in to Jenkins using with the username and password.
      Build the Jenkins Job called jenkinsmain.
  5. Notes:
  - Login into Jenkins env using the 'Login Jenkins Details' written below if you want to check the Jenkins Jobs
  - Go on Amazon AWS (profile = academy, region = eu-west-2) in order to see the items created, available and/or running and their properties
  - You can change most of the variables by duplicating the terraform.tfvars and substituting the values that you want to change


### **Login Jenkins Details** ###
  * Jenkins URL: www.jenkins.trio.academy.grads.al-labs.co.uk or the Jenkins public EIP appended by :8080.
  * username: admin
  * password: secret


### **Tools Used** ###
  * Editor for developing ~> Atom (languages Python & Shell, plug-in Git);
  * Provisioning Tool ~> Ansible
  * Environment Provisioning tools ~> Terraform
  * Communication ~> Slack; Google Meet
  * Project Management & Planning ~> Trello:
[https://trello.com/b/W3dOVIbE/happy-cloud]
[Project Plan - Google Docs]
[Pipeline Plan - Lucidchart]
[Process Diagram - Lucidchart]
[Building Diagram - Lucidchart]
  * Source/Version Control ~> Bitbucket & Git;
  * Cloud Computing Platform ~> Amazon AWS

### **Reference of external doc created** ###
  * Create a project plan -
  * Pipeline Plan - https://app.lucidchart.com/documents/edit/db315453-33b1-4d41-ac61-0ee50e69c4fd/25-1F5Z~nZ22?shared=true
  * Process Diagram - https://app.lucidchart.com/documents/edit/db315453-33b1-4d41-ac61-0ee50e69c4fd/D8-1m8nf4gje?shared=true
  * Building Diagram - https://app.lucidchart.com/documents/edit/db315453-33b1-4d41-ac61-0ee50e69c4fd/Hv_1q2SgkS0p?shared=true
