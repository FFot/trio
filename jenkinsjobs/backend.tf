# Add state to bucket
terraform {
  backend "s3" {
    bucket = var.s3_bucket
    key    = "${var.s3_bucket}.tfstate"
    region = var.aws_region
  }
}
