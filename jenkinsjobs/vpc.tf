#-------------Create VPC----------------

resource "aws_vpc" "trio_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "TrioVPC"
  }
}

#-----------Create VPC Subnets----------


#Public Management Subnet

resource "aws_subnet" "public_subnet_m" {
  vpc_id                  = aws_vpc.management_vpc.id
  cidr_block              = var.cidrs["publicM"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "PublicSubnetM"
  }
}

#Public Subnet ELB

resource "aws_subnet" "public_subnet_elb" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["publicelb"]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "PublicSubnetELB"
  }
}

#Private App Subnet 1

resource "aws_subnet" "private_subnet_app1" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["private1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "PrivateSubnetApp1"
  }
}

#Private App Subnet 2

resource "aws_subnet" "private_subnet_app2" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["private2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "PrivateSubnetApp2"
  }
}

#RDS Private Subnet 1

resource "aws_subnet" "rds_subnet_1" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["rds1"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "RDSSubnet1"
  }
}

#RDS Private Subnet 2

resource "aws_subnet" "rds_subnet_2" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["rds2"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "RDSSubnet2"
  }
}

#RDS Private Subnet 3

resource "aws_subnet" "rds_subnet_3" {
  vpc_id                  = aws_vpc.trio_vpc.id
  cidr_block              = var.cidrs["rds3"]
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[2]

  tags = {
    Name = "RDSSubnet3"
  }
}


#-------Create Internet gateway---------

resource "aws_internet_gateway" "trio_internet_gateway" {
  vpc_id = aws_vpc.trio_vpc.id

  tags = {
    Name = "TrioInternetGateway"
  }
}

#-------Create Route tables----------

#Public Route Table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.trio_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.trio_internet_gateway.id
  }

  tags = {
    Name = "PublicRouteTable"
  }
}

# Private Route Table
resource "aws_default_route_table" "private_rt" {
  default_route_table_id = aws_vpc.trio_vpc.default_route_table_id

  tags = {
    Name = "PrivateRouteTable"
  }
}

#------------Create Subnet Associations---------------

# ELB subnet Association with Public Route Table
resource "aws_route_table_association" "public_assoc_elb" {
  subnet_id      = aws_subnet.public_subnet_elb.id
  route_table_id = aws_route_table.public_rt.id
}

# App subnet 1 Association with Private Route Table
resource "aws_route_table_association" "private_assoc_2" {
  subnet_id      = aws_subnet.private_subnet_app1.id
  route_table_id = aws_default_route_table.private_rt.id
}

# App subnet 2 Association with Private Route Table
resource "aws_route_table_association" "private_assoc_2" {
  subnet_id      = aws_subnet.private_subnet_app2.id
  route_table_id = aws_default_route_table.private_rt.id
}
