##-------------------- Create DB -----------------------##
module "db" {

  snapshot_identifier = var.snapshotid == "" ? null : var.snapshotid

  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.0"

  identifier            = var.dbname
  copy_tags_to_snapshot = true
  publicly_accessible   = var.publicly_accessible

  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  storage_type      = var.storage_type
  allocated_storage = var.allocated_storage
  multi_az          = true

  name     = var.dbname
  username = var.dbusername
  password = var.dbpassword
  port     = var.port_mysql

  iam_database_authentication_enabled = false

  apply_immediately       = true
  backup_retention_period = var.backup_retention_period
  backup_window           = var.backup_window
  maintenance_window      = var.maintenance_window

  db_subnet_group_name   = var.db_subnet_group_name
  vpc_security_group_ids = [aws_security_group.db-sg.id]

# DB create parameter group
  family = "${var.engine}${var.maj_eng_ver}"
# DB option group
  major_engine_version = var.maj_eng_ver

  skip_final_snapshot       = var.skip_final_snapshot
  final_snapshot_identifier = var.dbname
  tags                      = var.tags-db

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}

##----------------- Create subnet group ----------------##
resource "aws_db_subnet_group" "db-subnet" {
  name       = var.db_subnet_group_name
  subnet_ids = [var.rds_subnet_1, var.rds_subnet_2, var.rds_subnet_3]
}

##---------------- Create DB schema --------------------##
resource "null_resource" "waitfordb" {
  provisioner "local-exec" {
    command = "until mysql -h ${module.db.this_db_instance_address} -u ${var.dbusername} -p${var.dbpassword} -e 'show databases'; do sleep 30; done"
  }
}

resource "null_resource" "clonerepo" {
  depends_on = [null_resource.waitfordb]
  provisioner "local-exec" {
    command = "git clone https://bitbucket.org/JangleFett/petclinic.git"
  }
}

resource "null_resource" "loadschema" {
  depends_on = [null_resource.waitfordb, null_resource.clonerepo]
  provisioner "local-exec" {
    command = "mysql -h ${module.db.this_db_instance_address} -u ${var.dbusername} -p${var.dbpassword} < petclinic/src/main/resources/db/mysql/schema.sql"
  }
}

resource "null_resource" "loaddata" {
  depends_on = [null_resource.loadschema]
  provisioner "local-exec" {
    command = "mysql -h ${module.db.this_db_instance_address} -u ${var.dbusername} -p${var.dbpassword} < petclinic/src/main/resources/db/mysql/data.sql"
  }
}
