
#-----------Route53-------------

#Primary (public) Host Zone

resource "aws_route53_zone" "primary" {
  name              = "${var.domain_name}.co.uk"
  delegation_set_id = var.delegation_set
}

# Petclinic CNAME

resource "aws_route53_record" "elb" {
  zone_id = aws_route53_zone.primary.zone_id
  name    = "www.petclinic.${var.domain_name}.co.uk"
  type    = "A"

  alias {
    name                   = aws_elb.elb.dns_name
    zone_id                = aws_elb.elb.zone_id
    evaluate_target_health = false
  }
}

#Secondary (private) Host Zone

resource "aws_route53_zone" "secondary" {
  name = "${var.domain_name}.co.uk"
  vpc {
    vpc_id = aws_vpc.trio_vpc.id
  }
}

#RDS CNAME

resource "aws_route53_record" "rds" {
  zone_id = "${aws_route53_zone.secondary.zone_id}"
  name    = "rds.${var.domain_name}.com"
  type    = "CNAME"
  ttl     = "300"
  records = ["${aws_db_instance._______.address}"]
}
