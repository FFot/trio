#----------Create ASG------------
resource "aws_autoscaling_group" "petclinic_asg" {
  name                      = "asg-${aws_launch_configuration.petclinic_lc.id}"
  max_size                  = var.asg_max
  min_size                  = var.asg_min
  health_check_grace_period = var.asg_grace
  health_check_type         = var.asg_hct
  desired_capacity          = var.asg_cap
  force_delete              = true
  load_balancers            = [aws_elb.elb.id]

  vpc_zone_identifier = [aws_subnet.private_subnet_app1.id,
    aws_subnet.wp_private_subnet_app2.id,
  ]

  launch_configuration = aws_launch_configuration.pc_lc.name

  tag {
    key                 = "Name"
    value               = "asg-instance"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}
