resource "aws_security_group_rule" "db-sg-rule-mysql" {
  from_port         = var.port_mysql
  protocol          = "tcp"
  security_group_id = aws_security_group.db-sg.id
  to_port           = var.port_mysql
  type              = "ingress"
  cidr_blocks       = var.cidr_blocks
}

resource "aws_security_group_rule" "db-sg-rule-ssh" {
  from_port         = var.port_ssh
  protocol          = "tcp"
  security_group_id = aws_security_group.db-sg.id
  to_port           = var.port_ssh
  type              = "ingress"
  cidr_blocks       = var.cidr_blocks
}

resource "aws_security_group_rule" "outbound_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.db-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
