#-----------Create Security Groups---------

#Public Security group

resource "aws_security_group" "public_sg" {
  name        = "PublicSecurityGroup"
  description = "Used for public and private instances for load balancer access"
  vpc_id      = aws_vpc.trio_vpc.id

  #HTTP

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #Outbound internet access

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Private Security Group

resource "aws_security_group" "private_sg" {
  name        = "PrivateSecurityGroup"
  description = "Used for private instances"
  vpc_id      = aws_vpc.trio_vpc.id

  # Access from other security groups

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#RDS Security Group

resource "aws_security_group" "rds_sg" {
  name        = "RDSSecurityGroup"
  description = "Used for Trio DB instances"
  vpc_id      = aws_vpc.trio_vpc.id

  # SQL access from public/private security group

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    security_groups = [
      aws_security_group.public_sg.id,
      aws_security_group.private_sg.id
    ]
  }
}
