#------------IAM ROLES----------------

#Create Jenkins access role profile

resource "aws_iam_instance_profile" "jenkins_access_profile" {
  name = "JenkinsAccessProfile"
  role = aws_iam_role.jenkins_access_role.name
}

#Create Jenkins role policy

resource "aws_iam_role_policy" "jenkins_access_policy" {
  name = "JenkinsAccessPolicy"
  role = aws_iam_role.jenkins_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
    "Effect": "Allow",
    "Action":[
        "s3:*",
        "ec2:*",
        "iam:*",
        "rds:*",
        "elasticloadbalancing:*",
        "autoscaling:*"
            ],
    "Resource": "*"
        }
    ]
}
EOF
}

#Create Jenkins access role

resource "aws_iam_role" "jenkins_access_role" {
  name = "JenkinsAccessRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
  },
      "Effect": "Allow",
      "Sid": ""
      }
    ]
}
EOF
}


#Create S3 access role profile

resource "aws_iam_instance_profile" "s3_access_profile" {
  name = "S3AccessProfile"
  role = aws_iam_role.s3_access_role.name
}

#Create role policy

resource "aws_iam_role_policy" "s3_access_policy" {
  name = "S3AccessPolicy"
  role = aws_iam_role.s3_access_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:*",
      "Resource": "*"
    }
  ]
}
EOF
}

#Create S3 access role attached to instance

resource "aws_iam_role" "s3_access_role" {
  name = "S3AccessRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
  {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
  },
      "Effect": "Allow",
      "Sid": ""
      }
    ]
}
EOF
}
