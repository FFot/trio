require 'spec_helper'

set :ssh_options, :user => 'ec2-user'

describe file('/usr/local/bin/vault') do
  it { should exist }
end

describe file('/etc/init.d/vault') do
  it { should exist }
end

describe service('vault') do
  it { should be_enabled }
  it { should be_running }
end

describe port(8200) do
  it { should be_listening }
end
